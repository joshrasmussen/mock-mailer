module mailer

go 1.14

require (
	github.com/emersion/go-smtp v0.13.0
	github.com/go-chi/chi v4.1.1+incompatible
)
