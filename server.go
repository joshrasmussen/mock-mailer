package main

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"sync"
	"time"

	"github.com/emersion/go-smtp"
	"github.com/go-chi/chi"
)

// Email keeps track of email data
type Email struct {
	From    string `json:"from"`
	To      string `json:"to"`
	Subject string `json:"subject"`
	Body    string `json:"body"`
}

// EmailLogAppender logs an email
type EmailLogAppender interface {
	AddEmail(email Email)
}

// A Session is returned after successful login.
type Session struct {
	Current *Email
	Logger  EmailLogAppender
}

// The Backend implements SMTP server methods.
type Backend struct {
	EmailList []Email
}

// AddEmail adds an email to our email list
func (bkd *Backend) AddEmail(email Email) {
	bkd.EmailList = append(bkd.EmailList, email)
}

// Login handles a login command with username and password.
func (bkd *Backend) Login(state *smtp.ConnectionState, username, password string) (smtp.Session, error) {
	session := &Session{
		Current: &Email{},
		Logger:  bkd,
	}

	return session, nil
}

// AnonymousLogin requires clients to authenticate using SMTP AUTH before sending emails
func (bkd *Backend) AnonymousLogin(state *smtp.ConnectionState) (smtp.Session, error) {
	return nil, smtp.ErrAuthRequired
}

// Mail handles who the email is being sent from
func (s *Session) Mail(from string, opts smtp.MailOptions) error {
	s.Current.From = from
	return nil
}

// Rcpt handles who the email is being sent to
func (s *Session) Rcpt(to string) error {
	s.Current.To = to
	return nil
}

// Data consumes the payload and parses out header and body
func (s *Session) Data(r io.Reader) error {
	b, err := ioutil.ReadAll(r)

	if err != nil {
		return err
	}

	str := strings.Split(string(b), "\n\n")

	headers := strings.Split(str[0], "\n")

	for _, line := range headers {
		parts := strings.Split(line, ": ")

		if parts[0] == "Subject" {
			s.Current.Subject = parts[1]
		}
	}

	s.Current.Body = str[1]

	return nil
}

// Reset Logs our email before we logout
func (s *Session) Reset() {
	s.Logger.AddEmail(*s.Current)
}

// Logout handles when the authenticated user logs out
func (s *Session) Logout() error {
	return nil
}

func startSMTP(be *Backend) {
	s := smtp.NewServer(be)

	s.Addr = ":587"
	s.Domain = "localhost"
	s.ReadTimeout = 30 * time.Second
	s.WriteTimeout = 10 * time.Second
	s.MaxMessageBytes = 1024 * 1024 // 1Kb
	s.MaxRecipients = 50
	s.AllowInsecureAuth = true

	log.Println("Starting SMTP server at", s.Addr)
	if err := s.ListenAndServe(); err != nil {
		log.Fatal(err)
	}
}

func startHTTP(be *Backend) {
	r := chi.NewRouter()

	r.Get("/emails", func(w http.ResponseWriter, r *http.Request) {
		data, err := json.Marshal(be.EmailList)

		if err != nil {
			http.Error(w, http.StatusText(500), 500)
			return
		}

		w.Write(data)
	})

	r.Delete("/emails", func(w http.ResponseWriter, r *http.Request) {
		be.EmailList = make([]Email, 0)
	})

	log.Println("Starting HTTP Server at port 8080")
	http.ListenAndServe(":8080", r)
}

func main() {
	wg := &sync.WaitGroup{}
	wg.Add(2)

	be := &Backend{
		EmailList: make([]Email, 0),
	}

	go func() {
		startSMTP(be)
		wg.Done()
	}()

	go func() {
		startHTTP(be)
		wg.Done()
	}()

	wg.Wait()
}
