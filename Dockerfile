FROM golang:latest as builder
COPY . /workspace
WORKDIR /workspace
RUN go mod download
RUN CGO_ENABLED=0 go build -o server server.go


FROM alpine:latest
WORKDIR /workspace
COPY --from=builder /workspace/server .
EXPOSE 8080
EXPOSE 587
CMD ["./server"]
